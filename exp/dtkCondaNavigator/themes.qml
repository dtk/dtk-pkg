import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Page {
    id: page

    Flickable {
        anchors.fill: parent
        contentHeight: pane.implicitHeight
        flickableDirection: Flickable.AutoFlickIfNeeded

        Pane {
            id: pane

            anchors.fill: parent;

            contentItem:  Column {

                anchors.fill: parent;

                ComboBox {
                    id: themeChooser;

                    model: theme.themes();

                    width: parent.width;

                    onActivated: {
                        theme.apply(themeChooser.textAt(index));
                    }
                }

                Button {

                    text: "Done";

                    width: parent.width;

                    onClicked: {
                        stackView.pop();
                    }
                }
            }
        }
    }
}
