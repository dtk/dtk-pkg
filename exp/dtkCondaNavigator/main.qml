// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Controls.Universal 2.12

import Qt.labs.settings 1.0

// /////////////////////////////////////////////////////////////////////////////
// Oh yeah
// /////////////////////////////////////////////////////////////////////////////

import dtk.Store 1.0

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

ApplicationWindow {

    id: window;

    width: 1024;
    height: 600;

    visible: true;

    title: "dtkPkg Conda Navigator";

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    Material.theme: theme.dark ? Material.Dark : Material.Light;
    Material.background: theme.bg;
    Material.foreground: theme.fg;
    Material.primary: theme.fgalt;
    Material.accent: theme.hl;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    Settings {

        id: settings;

        // property alias key: id.value;
    }

// /////////////////////////////////////////////////////////////////////////////
// Logic
// /////////////////////////////////////////////////////////////////////////////

    StoreModel {
        id: storemodel;
        type: StoreModel.Envs;

        Component.onCompleted: {
            storemodel.setDocument(log.document);
            storemodel.exec();
        }
    }

// /////////////////////////////////////////////////////////////////////////////
// Decoration
// /////////////////////////////////////////////////////////////////////////////

    Action {
        id: settingsAction
        // icon.name: stackView.depth > 1 ? "back" : "drawer"
        //
        icon.name: "settings";
        icon.source: "gear.png";
        //
        onTriggered: {
            drawer.open()
        }
    }

    header: ToolBar {

        Material.foreground: theme.bgalt;

        RowLayout {
            spacing: 20
            anchors.fill: parent

            ToolButton {
                action: settingsAction;
            }

            Label {
                id: titleLabel
                text: "dtkPkg Conda Navigator"
                font.pixelSize: 20
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
        }
    }

    property int index: 0;

    footer: TabBar {

        id: bar;

        TabButton {
            text: "Envs";

            onClicked: {
                window.index = 0;
                
                storemodel.setType(StoreModel.Envs);
                storemodel.exec();
            }
        }

        TabButton {
            text: "Info";

            onClicked: {
                window.index = 1;
                
                storemodel.setType(StoreModel.Info);
                storemodel.exec();
            }
        }

        TabButton {
            text: "List";

            onClicked: {
                window.index = 2;

                storemodel.setType(StoreModel.List);
                storemodel.exec();
            }
        }

        TabButton {
            text: "Search";

            onClicked: {
                window.index = 3;

                storemodel.setType(StoreModel.Search);
                if(search.text.length) {
                    storemodel.setPackage(search.text);
                    storemodel.exec();
                } else {
                    storemodel.clear();
                }
            }
        }
    }

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    Drawer {
        id: drawer
        width: Math.min(window.width, window.height) / 3 * 2
        height: window.height
        interactive: true;

        ListView {
            id: listView

            focus: true
            currentIndex: -1
            anchors.fill: parent

            delegate: ItemDelegate {
                width: parent.width
                text: model.title
                highlighted: ListView.isCurrentItem
                onClicked: {
                    listView.currentIndex = index;
                    stackView.push(model.source);
                    drawer.close();
                }
            }

            model: ListModel {
                ListElement { title: "Themes"; source: "qrc:/themes.qml" }
            }

            ScrollIndicator.vertical: ScrollIndicator { }
        }
    }

// /////////////////////////////////////////////////////////////////////////////
// Ux
// /////////////////////////////////////////////////////////////////////////////

    StackView {
        id: stackView
        anchors.fill: parent;
        anchors.margins: 0;

        spacing: 0;

        initialItem: Pane {

            id: pane

            ColumnLayout {

                anchors.fill: parent;
                anchors.margins: 0;

                spacing: 0;

                TextField {
                    id: search;

                    enabled: window.index == 3;
                    visible: window.index == 3;

                    placeholderText: "Package...";

                    focus: true;

                    padding: 10;

                    onAccepted: {
                        storemodel.setType(StoreModel.Search);
                        storemodel.setPackage(search.text);
                        storemodel.exec();
                    }

                    Layout.fillWidth: true;
                }

                ListView {
                    id: view;

                    clip: true;

                    model: storemodel;

                    anchors.margins: 0;

                    delegate: Rectangle {

                        id: delegate;

                        property color bg_color: theme.bgalt;
                        property color fg_color: theme.fgalt;

                        property bool hovered: false;

                        color: bg_color;

                        width: parent.width;

                        MouseArea {

                            id: rowArea;

                            anchors.fill: parent;

                            hoverEnabled: true;

                            onEntered: {

                                delegate.bg_color = theme.bg;
                                delegate.fg_color = theme.fg;
                                delegate.hovered = true;
                            }

                            onExited: {
                                delegate.bg_color = theme.bgalt;
                                delegate.fg_color = theme.fgalt;
                                delegate.hovered = false;
                            }
                        }

                        Row {

                            id: row;

                            property bool envCurrent: false;
                            property string env: '';

                            anchors.fill: parent;

                            Text {
                                text: name.replace('*', '');

                                verticalAlignment: Qt.AlignVCenter;
                                padding: 7;

                                width: parent.width/4;

                                color: delegate.fg_color;

                                elide: Text.ElideRight;

                                Rectangle {
                                    anchors.top: parent.top;
                                    anchors.right: parent.right;
                                    anchors.bottom: parent.bottom;
                                    width: 1;
                                    color: theme.bd;
                                }

                                Component.onCompleted: {
                                    row.envCurrent = name.endsWith("*");
                                    row.env = name;
                                }
                            }

                            Text {
                                text: version;

                                verticalAlignment: Qt.AlignVCenter;
                                padding: 7;

                                width: build.length ? parent.width/4 : 3*parent.width/4;

                                color: delegate.fg_color;

                                elide: Text.ElideRight;

                                // /////////////////////////////////////////////////////////////////////////////
                                // Activate/deactivate button
                                // /////////////////////////////////////////////////////////////////////////////

                                Rectangle {

                                    color: "orange";

                                    width: parent.width/(3*2*2);
                                    height: parent.height;

                                    anchors.verticalCenter: parent.verticalCenter;
                                    anchors.right: parent.right;

                                    visible: delegate.hovered && window.index === 0 && row.envCurrent;

                                    Text {

                                        anchors.centerIn: parent;

                                        text: row.envCurrent ? "Current" : "";

                                        color: "white";

                                        font.pointSize: 8;
                                    }
                                }

                                // /////////////////////////////////////////////////////////////////////////////

                                Rectangle {
                                    anchors.top: parent.top;
                                    anchors.right: parent.right;
                                    anchors.bottom: parent.bottom;
                                    width: 1;
                                    color: theme.bd;
                                }
                            }

                            Text {
                                text: build;

                                verticalAlignment: Qt.AlignVCenter;
                                padding: 7;

                                width: parent.width/4;

                                color: delegate.fg_color;

                                elide: Text.ElideRight;

                                Rectangle {
                                    anchors.top: parent.top;
                                    anchors.right: parent.right;
                                    anchors.bottom: parent.bottom;
                                    width: 1;
                                    color: theme.bd;

                                    visible: (build.length > 0);
                                }
                            }

                            Text {
                                text: channel;

                                verticalAlignment: Qt.AlignVCenter;
                                padding: 7;

                                width: parent.width/4;

                                color: delegate.fg_color;

                                elide: Text.ElideRight;

                                // /////////////////////////////////////////////////////////////////////////////
                                // Install/Renove button
                                // /////////////////////////////////////////////////////////////////////////////

                                Rectangle {

                                    width: parent.width/3;
                                    height: parent.height;

                                    anchors.verticalCenter: parent.verticalCenter;
                                    anchors.right: parent.right;

                                    visible: delegate.hovered && window.index === 3;

                                    Text {

                                        anchors.centerIn: parent;

                                        color: "white";

                                        font.pointSize: 8;

                                        text: storemodel.installed(name + ' ' + version + ' ' + build) ? "Remove" : "Install";
                                    }

                                    MouseArea {
                                        anchors.fill: parent;

                                        z: rowArea.z + 100;

                                        onClicked: {
                                            storemodel.installed(name + ' ' + version + ' ' + build) ?
                                                storemodel.remove(name + "=" + version) :
                                                storemodel.install(name + "=" + version);
                                        }
                                    }

                                    color: storemodel.installed(name + ' ' + version + ' ' + build) ? theme.red : theme.green;
                                }
                            }
                        }

                        Rectangle {
                            anchors.left: parent.left;
                            anchors.right: parent.right;
                            anchors.bottom: parent.bottom;
                            height: 1;
                            color: theme.bd;
                        }

                        height: 30;
                    }

                    Layout.alignment: Qt.AlignTop;
                    Layout.fillWidth: true;
                    Layout.fillHeight: true;
                    Layout.margins: 0;

                    ScrollIndicator.vertical: ScrollIndicator {}
                }

                Rectangle {

                    enabled: window.index == 3;
                    visible: window.index == 3;

                    Layout.alignment: Qt.AlignTop;
                    Layout.fillWidth: true;

                    Layout.minimumHeight: parent.height / 4;
                    Layout.preferredHeight: parent.height / 4;

                    Log {
                        id: log;

                        anchors.fill: parent;
                    }

                    Rectangle {
                        anchors.left: parent.left;
                        anchors.right: parent.right;
                        anchors.top: parent.top;
                        height: 1;
                        color: theme.bd;
                    }
                }
            }
        }
    }
}

//
// main.qml ends here
