import QtQuick 2.12
import QtQuick.Controls 2.12

Rectangle {
    id: root;

    property alias text: _textEdit.text;
    property alias document: _textEdit.textDocument;

    clip: true;

    color: theme.bgalt;

    Flickable {
        id: _flickable;

        anchors.left: _lines.right;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;
        anchors.right: parent.right;

        contentWidth: _textEdit.width;
        contentHeight: _textEdit.height;

        boundsBehavior: Flickable.StopAtBounds

        function updateScrollX(x) {
            if (contentX >= x) {
                contentX = x;
            } else if (contentX + width <= x) {
                contentX = x + 1 - width;
            }
        }

        function updateScrollY(y) {
            if (contentY >= y) {
                contentY = y;
            } else if (contentY + height <= y + _textEdit.cursorHeight) {
                contentY = y + _textEdit.cursorHeight - height;
            }
        }

        Rectangle {
            anchors.left: parent.left;
            anchors.right: parent.right;

            color: theme.bgalt;

            y: _textEdit.cursorY;
            height: _textEdit.cursorHeight;
        }

        TextEdit {
            id: _textEdit;

            objectName: "textEditor";

            readOnly: true;
            font.family: "Source Code Pro";

            selectByMouse: true
            mouseSelectionMode: TextEdit.SelectCharacters
            selectionColor: "#aa4D69A8"
            color: theme.fg;

            textFormat: TextEdit.PlainText
            wrapMode: TextEdit.NoWrap

            width: Math.max(implicitWidth, root.width - _lines.width);
            height: Math.max(implicitHeight, root.height);

            leftPadding: 4;

            property int cursorX: cursorRectangle.x;
            property int cursorY: cursorRectangle.y;
            property int cursorHeight: cursorRectangle.height;
            property bool showCursor: true;

            font.pointSize: 13;

            onCursorXChanged: {
                _flickable.updateScrollX(cursorX);
            }

            onCursorYChanged: {
                _flickable.updateScrollY(cursorY);
            }

            cursorDelegate: Rectangle {
                width: 1.5;
                color: "orange";
                visible: _textEdit.showCursor;
            }
        }
    }

    Item {
        id: _lines;

        visible: false;

        anchors.left: parent.left;
        anchors.top: parent.top;
        anchors.bottom: parent.bottom;

        width: _linesCol.width + 4;

        Column {
            id: _linesCol;

            y: -_flickable.contentY;

            Repeater {
                model: _textEdit.lineCount;

                delegate: Text {
                    anchors.right: parent.right;
                    rightPadding: 8;
                    leftPadding: 8;

                    font.family: _textEdit.font.family;
                    font.pointSize: _textEdit.font.pointSize;

                    verticalAlignment: Text.AlignVCenter;

                    text: index + 1;
                }
            }
        }

        Rectangle {
            anchors.top: parent.top;
            anchors.bottom: parent.bottom;
            anchors.right: parent.right;

            width: 1;
            color: "#21252B"
        }
    }
}
