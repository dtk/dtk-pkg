// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtQuick>
#include <QtQuickControls2>

#include <dtkFonts>
#include <dtkThemes>
#include <dtkStore>

// /////////////////////////////////////////////////////////////////////////////
// Helper functions
// /////////////////////////////////////////////////////////////////////////////

template <class T> QList<T> childObjects(QQmlApplicationEngine *engine, const QString& objectName, const QString& propertyName)
{
    QList<QObject*> rootObjects = engine->rootObjects();

    QList<T> children;

    foreach (QObject* object, rootObjects) {

        foreach(QObject* child, object->findChildren<QObject*>(objectName)) {

            if (child != 0) {
                std::string s = propertyName.toStdString();
                QObject* object = child->property(s.c_str()).value<QObject*>();
                Q_ASSERT(object != 0);
                T prop = dynamic_cast<T>(object);
                Q_ASSERT(prop != 0);
                children << prop;
            }
        }
    }

    return children;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setApplicationName("conda-navigator");
    QCoreApplication::setOrganizationName("inria");
    QCoreApplication::setOrganizationDomain("fr");

    QGuiApplication application(argc, argv);

    // Fonts initialisation

    dtkFontAwesome::instance()->initFontAwesome();
    dtkFontSourceCodePro::instance()->initFontSourceCodePro();

    // Themes initialisation

    dtkThemesEngine::instance()->apply();

    // Store initialisation

    dtkStoreModel::bind();

    // --

    QQuickStyle::setStyle("Material");
   
    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:/");
    engine.rootContext()->setContextProperty("font", dtkFontAwesome::instance());
    engine.rootContext()->setContextProperty("theme", dtkThemesEngine::instance());
    engine.load("qrc:/main.qml");

    dtkThemesEngine::instance()->apply();

    return application.exec();
}

//
// main.cpp ends here
