// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkStoreModel.h"
#include "dtkStoreProcess.h"

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkStoreModelPrivate
{
public:
    void sync(void);

public:
    dtkStoreProcess *process = nullptr;
    dtkStoreModel *q;

public:
    QStringList installed;
};

void dtkStoreModelPrivate::sync(void)
{
    if(!this->process)
        this->process = new dtkStoreProcess(q);

    this->process->setType(dtkStoreProcess::List);
    this->process->exec();

    this->installed = this->process->output();

    for(int i = 0; i < this->installed.count(); i++)
        this->installed[i] = this->installed[i].simplified();
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

dtkStoreModel::dtkStoreModel(QObject *parent) : QAbstractItemModel(parent)
{
    d = new dtkStoreModelPrivate;
    d->q = this;

    d->sync();
}

dtkStoreModel::~dtkStoreModel(void)
{
    delete d;
}

void dtkStoreModel::addChannel(const QString& channel)
{
    d->process->addChannel(channel);
}

void dtkStoreModel::remChannel(const QString& channel)
{
    d->process->remChannel(channel);
}

void dtkStoreModel::clearChannels(void)
{
    d->process->clearChannels();
}

void dtkStoreModel::setEnvironment(QString environment)
{
    this->beginResetModel();

    d->process->setEnvironment(environment);

    this->endResetModel();
}

void dtkStoreModel::setPackage(const QString& package)
{
    d->process->setPackage(package);
}

void dtkStoreModel::setType(dtkStoreModel::Type type)
{
    d->process->setType(static_cast<dtkStoreProcess::Type>(type));

    emit typeChanged();
}

void dtkStoreModel::install(const QString& package)
{
    this->beginResetModel();

    d->process->install(package);

    d->sync();

    d->process->setType(dtkStoreProcess::Search);
    d->process->exec();

    this->endResetModel();
}

void dtkStoreModel::remove(const QString& package)
{
    this->beginResetModel();

    d->process->remove(package);

    d->sync();

    d->process->setType(dtkStoreProcess::Search);
    d->process->exec();

    this->endResetModel();
}

void dtkStoreModel::clear(void)
{
    this->beginResetModel();

    d->process->clear();

    this->endResetModel();
}

void dtkStoreModel::exec(void)
{
    this->beginResetModel();

    d->process->exec();

    this->endResetModel();
}

dtkStoreModel::Type dtkStoreModel::type(void)
{
    return static_cast<dtkStoreModel::Type>(d->process->type());
}

bool dtkStoreModel::installed(const QString& package)
{
    return d->installed.contains(package);
}

int dtkStoreModel::rowCount(const QModelIndex& parent) const
{
    return d->process->output().count();
}

int dtkStoreModel::columnCount(const QModelIndex &parent) const
{
    switch(d->process->type()) {
    case dtkStoreProcess::Envs:
    case dtkStoreProcess::Info:
        return 2;
    case dtkStoreProcess::List:
        return 3;
    case dtkStoreProcess::Search:
        return 4;
    default:
        return 0;
    }
}

QVariant dtkStoreModel::data(const QModelIndex &index, int role) const
{
    if(d->process->output().at(index.row()).simplified().split(QRegExp("\\s+")).count() < 2) {
        if(role == VersionRole) {
            return d->process->output().at(index.row()).simplified();
        } else {
            return QString();
        }
    }

    switch (role) {
    case NameRole:
        if(d->process->type() == dtkStoreProcess::Info)
            return d->process->output().at(index.row()).simplified().split(QRegExp(" : ")).at(0);
        else
            return d->process->output().at(index.row()).simplified().split(QRegExp("\\s+")).at(0);
    case VersionRole:
        if(d->process->type() == dtkStoreProcess::Info)
            return d->process->output().at(index.row()).simplified().split(QRegExp(" : ")).at(1);
        else
            return d->process->output().at(index.row()).simplified().split(QRegExp("\\s+")).at(1);
    case BuildRole:
        if(d->process->type() == dtkStoreProcess::None || d->process->type() == dtkStoreProcess::Info || d->process->type() == dtkStoreProcess::Envs)
            break;
        return d->process->output().at(index.row()).simplified().split(QRegExp("\\s+")).at(2);
    case ChannelRole:
        if(d->process->type() == dtkStoreProcess::None || d->process->type() == dtkStoreProcess::Info || d->process->type() == dtkStoreProcess::Envs)
            break;
        if(d->process->output().at(index.row()).simplified().split(QRegExp("\\s+")).count() < 4)
            return "";
        else
            return d->process->output().at(index.row()).simplified().split(QRegExp("\\s+")).at(3);
    default:
        break;
    };

    return QString();
}

QModelIndex dtkStoreModel::index(int row, int column, const QModelIndex &parent) const
{
    return createIndex(row, column);
}

QModelIndex dtkStoreModel::parent(const QModelIndex &index) const
{
    return QModelIndex();
}

QHash<int, QByteArray> dtkStoreModel::roleNames(void) const
{
    return {
        {   NameRole, "name"},
        {VersionRole, "version"},
        {  BuildRole, "build"},
        {ChannelRole, "channel"}
    };
}

void dtkStoreModel::setDocument(QQuickTextDocument *document)
{
    d->process->setDocument(document);
}

//
// dtkStoreModel.cpp ends here
