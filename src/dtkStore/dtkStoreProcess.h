// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkStoreExport>

#include <QtCore>
#include <QtGui>
#include <QtQml>

#include <QtQuick>


#include <QtQuickControls2>

class DTKSTORE_EXPORT dtkStoreProcess : private QProcess
{
    Q_OBJECT

public:
    enum Type {
        None = 0,
        Envs,
        Info,
        List,
        Search
    };

    Q_ENUM(Type)

public:
     dtkStoreProcess(QObject *parent = nullptr);
    ~dtkStoreProcess(void);

public:
    Type type(void);

public:
    void addChannel(const QString&);
    void remChannel(const QString&);
    void clearChannels(void);

public:
    void setEnvironment(const QString&);
    void setPackage(const QString&);

public:
    void setDocument(QQuickTextDocument *);
    void setType(Type);

public slots:
    void install(const QString&);
    void remove(const QString&);

public slots:
    void clear(void);
    void exec(void);

public:
    const QStringList& output(void);

private:
    class dtkStoreProcessPrivate *d;

private:
    friend class dtkStoreProcessPrivate;
};

//
// dtkStoreProcess.h ends here
