// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <dtkStoreExport>

#include <QtCore>
#include <QtGui>
#include <QtQml>
#include <QtQuick>
#include <QtQuickControls2>

class DTKSTORE_EXPORT dtkStoreModel : public QAbstractItemModel
{
    Q_OBJECT
    Q_PROPERTY(Type type READ type WRITE setType NOTIFY typeChanged)

public:
    enum Type {
        None = 0,
        Envs,
        Info,
        List,
        Search
    };

    enum Roles {
           NameRole = Qt::UserRole + 1,
        VersionRole,
          BuildRole,
        ChannelRole
    };

    Q_ENUM(Type)
    Q_ENUM(Roles)

public:
     dtkStoreModel(QObject *parent = nullptr);
    ~dtkStoreModel(void);

signals:
    void typeChanged(void);

public:
    Q_INVOKABLE void addChannel(const QString&);
    Q_INVOKABLE void remChannel(const QString&);
    Q_INVOKABLE void clearChannels(void);

public slots:
    Q_INVOKABLE void setEnvironment(QString);
    Q_INVOKABLE void setPackage(const QString&);

public slots:
    void setType(Type);

public slots:
    void install(const QString&);
    void remove(const QString&);
    void clear(void);
    void exec(void);

public:
    Type type(void);

public:
    Q_INVOKABLE bool installed(const QString&);

public:
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    virtual QModelIndex parent(const QModelIndex &index) const override;
    virtual QHash<int, QByteArray> roleNames(void) const override;

public:
    Q_INVOKABLE void setDocument(QQuickTextDocument *);

public:
    static void bind(void) {
        qmlRegisterType<dtkStoreModel>("dtk.Store", 1, 0, "StoreModel");
    }

private:
    class dtkStoreModelPrivate *d;
};

//
// dtkStoreModel.h ends here
