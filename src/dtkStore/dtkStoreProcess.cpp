// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkStoreProcess.h"

#include <QtConcurrent>
#include <QtDebug>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class dtkStoreProcessPrivate
{
public:
    void envs(void);
    void info(void);
    void list(void);
    void search(void);
    void install(const QString&);
    void remove(const QString&);

public:
    void exec(void);
   
public:
    dtkStoreProcess::Type type;

public:
    const QString program = "conda";

public:
    QStringList arguments;

public:
    QString package;
    QString query;

public:
    QStringList output;

public:
    QTextDocument *document = nullptr;

public:
    bool installing = false;

public:
    dtkStoreProcess *q;
};

void dtkStoreProcessPrivate::exec(void)
{
    switch(type) {
        case dtkStoreProcess::Envs:
        this->envs();
        break;
    case dtkStoreProcess::Info:
        this->info();
        break;
    case dtkStoreProcess::List:
        this->list();
        break;
    case dtkStoreProcess::Search:
        this->search();
        break;
    default:
        qDebug() << Q_FUNC_INFO << "Invalid process type";
    }
}

void dtkStoreProcessPrivate::envs(void)
{
    QStringList args;
    args << "env" << "list";

    q->start(program, args);
    q->waitForFinished();

    output = QString(q->readAll()).replace(QRegExp("\\s+\\*"), "*").split("\n");
    output.takeFirst();
    output.takeFirst();
    output.takeLast();
    output.takeLast();
}

void dtkStoreProcessPrivate::info(void)
{
    QStringList args;
    args << "info";

    q->start(program, args);
    q->waitForFinished();

    output = QString(q->readAll()).split("\n");
    output.takeFirst();
    output.takeLast();
    output.takeLast();
}

void dtkStoreProcessPrivate::list(void)
{
    QStringList args;
    args << "list";

    q->start(program, args);
    q->waitForFinished();

    output = QString(q->readAll()).split("\n");
    output.takeFirst();
    output.takeFirst();
    output.takeFirst();
    output.takeLast();
}

void dtkStoreProcessPrivate::search(void)
{
    QStringList args;
    args << "search";
    args << package;

    q->start(program, args);
    q->waitForFinished();

    output = QString(q->readAll()).split("\n");
    output.takeFirst();
    output.takeFirst();
    output.takeLast();

    query = package;
}

void dtkStoreProcessPrivate::install(const QString& package)
{
    if (document)
        document->clear();

    installing = true;

    QStringList args;
    args << "install";
    args << "-y";
    args << package;

    q->start(program, args);
    q->waitForFinished();

    installing = false;
}

void dtkStoreProcessPrivate::remove(const QString&)
{
    if (document)
        document->clear();

    installing = true;

    QStringList args;
    args << "remove";
    args << "-y";
    args << package;

    q->start(program, args);
    q->waitForFinished();

    installing = false;
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

dtkStoreProcess::dtkStoreProcess(QObject *parent) : QProcess(parent)
{
    d = new dtkStoreProcessPrivate; d->q = this;
}

dtkStoreProcess::~dtkStoreProcess(void)
{
    delete d;
}

dtkStoreProcess::Type dtkStoreProcess::type(void)
{
    return d->type;
}

void dtkStoreProcess::addChannel(const QString& channel)
{
    d->arguments << QString("-c %1").arg(channel);
}

void dtkStoreProcess::remChannel(const QString& channel)
{
    d->arguments.removeAll(QString("-c %1").arg(channel));
}

void dtkStoreProcess::clearChannels(void)
{
    d->arguments.clear();
}

void dtkStoreProcess::setEnvironment(const QString& environment)
{
    QStringList args;

    if(!environment.isEmpty())
        args << "activate";
    else
        args << "deactivate";

    if(!environment.isEmpty())
        args << environment;

    this->start(d->program, args);
    this->waitForFinished();
}

void dtkStoreProcess::setPackage(const QString& package)
{
    d->package = package;
}

void dtkStoreProcess::setDocument(QQuickTextDocument *document)
{
    d->document = document->textDocument();

    connect(this, &QProcess::readyReadStandardOutput, [=] ()
    {
        if (d->installing) {
            d->document->setPlainText(d->document->toPlainText() + this->readAllStandardOutput());
        }
    });
}

void dtkStoreProcess::setType(dtkStoreProcess::Type type)
{
    d->type = type;
}

void dtkStoreProcess::install(const QString& package)
{
    // QtConcurrent::run(d, &dtkStoreProcessPrivate::install, package);

    d->install(package);
}

void dtkStoreProcess::remove(const QString& package)
{
    // QtConcurrent::run(d, &dtkStoreProcessPrivate::install, package);

    d->remove(package);
}

void dtkStoreProcess::clear(void)
{
    d->output.clear();
}

void dtkStoreProcess::exec(void)
{
    d->exec();
}

const QStringList& dtkStoreProcess::output(void)
{
    return d->output;
}

//
// dtkStoreProcess.cpp ends here
